import React from 'react';
import './App.css';

import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom" 

import Sidebar from './components/Sidebar/Sidebar';
import AboutMe from './components/AboutMe/AboutMe';
import WhatIDo from './components/WhatIDo/WhatIDo';
import Resume from './components/Resume/Resume'
import Portfolio from './components/Portfolio/Portfolio';

function App() {
  return (
    <div className="app">
      <Router>
        <div className="app__sidebar">
          <Sidebar/>
        </div>

        <div className="app_content">
          <Switch>

            <Route exact path="/" component={AboutMe}/>

            <Route exact path="/whatido" component={WhatIDo}/>

            <Route exact path="/resume" component={Resume}/>

            <Route exact path="/portfolio" component={Portfolio}/>

            {/* Redirect all 404's to home */}
            <Redirect to='/*' />

          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
