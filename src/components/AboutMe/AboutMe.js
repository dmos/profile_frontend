import { Button } from '@material-ui/core'
import React from 'react'
import "./AboutMe.css"

import portfolio from '../../static/Current_CV.pdf'

function AboutMe() {
    return (
        <div className="aboutme">
            <div className="aboutme__name">
                <span>Diogo Santos</span>
                <br/> Developer
            </div>

            <div className="aboutme__info">
                
                {/* <div className="abouteme__info_desc">
                    
                </div> */}

                <div className="aboutme__info__ul">
                    <ul>
                        <li><span>Name: </span> Diogo Miguel Oliveira Santos</li>
                        <li><span>Age: </span> 22</li>
                        <li><span>Email: </span> dmos999@gmail.com</li>
                        <li><span>Contact: </span> (+351) 911 114 531</li>
                        <li><span>Living in: </span> Peterborough, England</li>
                        <li><span>From: </span> Aveiro, Portugal</li>
                    </ul>
                </div>

            </div>

            <div className="aboutme__download">
                <a href={portfolio} target="_blank">
                    <Button>Download CV</Button>
                </a>
            </div>

        </div>
    )
}


export default AboutMe
