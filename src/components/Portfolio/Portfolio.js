import React, {useEffect,useState} from 'react'
import "./Portfolio.css"

import {FaGitlab} from 'react-icons/fa'


import {portfolioData} from '../../static/data'
import Requests from '../../requests'

function Portfolio() {

    const [Apps, setApps] = useState([]);

    useEffect(() => {
        setApps(portfolioData);

        fetch(Requests.portfolio)
        .then(res => res.json)
        .then(
            (result) => {setApps(result)},
            (error) => {console.error(error);}
        );

    }, []);

    return (
        <div className="portfolio">
            <div className="__title">
                Portfolio
            </div>
            
            <div className="portfolio__appsList">
                {
                    Apps.map(item =>(
                        <div key={item.appName} className="portfolio__app">
                            <img src={process.env.PUBLIC_URL +item.image}  ></img>
                            <h2> {item.appName} </h2>
                            <h3> {item.year} </h3>
                            <p> {item.description} </p>
                            <a href={item.repoLink} target="_blank" rel="noopener noreferrer">
                                Link to Repository
                                <FaGitlab />
                            </a>

                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Portfolio
