import React, { useEffect, useState } from 'react'
import "./Resume.css"


import ResumeCard from './ResumeCard';

import Requests from '../../requests'
import { workData, studyData } from '../../static/data.js'

function Resume() {

    const [studies, setStudies] = useState([]);
    const [work, setWork] = useState([]);
    
    useEffect(() => {
        setStudies(studyData)
        setWork(workData);

        fetch(Requests.studies)
        .then(res => res.json())
        .then(
            (result)=> {setStudies(result)},
            (error) => {console.error(error)}           
        );
        
        fetch(Requests.work)
        .then(res => res.json())
        .then(
            (result) => { setWork(result) },
            (error) => {console.error(error)}
        );

    }, [])



    return (
        <div className="resume">
            <div className="__title">
                Resume
            </div>

            <div className="resume__list">
                
                <div className="resume__list__column">
                    <h2 className="__subtitle">
                        My Experience
                    </h2>
                    {
                        work.map(item=> (
                            <ResumeCard key={item._id} values={item}/>
                        ))
                    }

                </div>

                <div className="resume__list__column">
                    <h2 className="__subtitle">
                        My Education
                    </h2>
                    {
                        studies.map(item => (
                            <ResumeCard key={item._id} values={item} education/>
                        ))
                    }

                </div>
            </div>
        </div>
    )
}

export default Resume
