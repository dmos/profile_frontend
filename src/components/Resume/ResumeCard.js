import React from 'react'
import './ResumeCard.css'

import Chip from '@material-ui/core/Chip';

function ResumeCard({education, values}) {

    const formateDates = (startDate, endDate)=>{
        startDate = new Date(startDate).toLocaleDateString("en-UK", { month: 'long', year: 'numeric' });
        if(!endDate){
            return startDate
        }
        endDate = new Date(endDate).toLocaleDateString("en-UK", { month: 'long', year: 'numeric' });
        return startDate + " - " + endDate
    }

    return(
        <div className="experience__card">
            <Chip label={formateDates(values.startDate,values.endDate)} className="dates" />
            <h3>{education ? values.curseName : values.jobTitle}</h3>
            <h4>{education ? values.place : values.company}</h4>
            <p>{values.description}</p>
            {values.skills && values.skills.length>0 
            ?   
                    values.skills.map((item) => <Chip label={item} key={item} className="skills"/>)
                
            : ""}
        </div>
    )
    
}

export default ResumeCard
