import React from 'react'
import './Sidebar.css'

import {Avatar} from '@material-ui/core'
import { NavLink  } from "react-router-dom"

import { FaLinkedin, FaGitlab} from 'react-icons/fa'
import { GrMail} from 'react-icons/gr'

import image from '../../static/image.jpg'

function Sidebar() {
    return (
        <div className="sidebar">

            {/* ME */}
            <Avatar
                alt="Remy Sharp"
                src={image}
                className="sidebar__avatar"
            />

            <div className="sidebar__name">
                Diogo Santos
            </div>

            {/* links */}
            <div className="sidebar__links">
                <NavLink exact to="/">
                    about me
                </NavLink>

                <NavLink exact to="/whatido">
                    what i do
                </NavLink>

                <NavLink exact to="/resume">
                    resume
                </NavLink>

                <NavLink exact to="/portfolio">
                    portfolio
                </NavLink>

            </div>
            

            {/* social */}
            <div className="sidebar__social">
                <a href="https://www.linkedin.com/in/DiogoMOSantos" target="_blank" rel="noopener noreferrer"><FaLinkedin /></a>
                <a href="https://gitlab.com/dmos" target="_blank" rel="noopener noreferrer"><FaGitlab /></a>
                <a href="mailto:dmos999@gmail.com" target="_blank" rel="noopener noreferrer"><GrMail/></a>
            </div>

        </div>
    )
}

export default Sidebar
