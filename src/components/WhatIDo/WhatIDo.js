import React from 'react'
import "./WhatIDo.css"

import { FaDatabase, FaServer, FaMobileAlt, FaDesktop } from 'react-icons/fa'

function WhatIDo() {
    return (
        <div className="whatido">
            <div className="__title">
                What i do?
            </div>

            <div className="whatido__row">
                <div className="whatido__card">
                    <div className="whatido__card__icon">
                        <FaDatabase />
                    </div>
                    <div className="whatido__card__text">
                        <h3>Database administration</h3>
                        PostgreSQL, MySQL and SQLite
                    </div>
                </div>

                <div className="whatido__card">
                    <div className="whatido__card__icon">
                        <FaServer/>
                    </div>
                    <div className="whatido__card__text">
                        <h3>Backend</h3>
                        PHP, Laravel, NodeJS, Express, REST, SOAP, Java
                    </div>
                </div>

                <div className="whatido__card">
                    <div className="whatido__card__icon">
                        <FaMobileAlt />
                    </div>
                    <div className="whatido__card__text">
                        <h3>Frontend</h3>
                        HTML, CSS, Bootstrap, Materialize, JavaScript, Angular, React, Android
                    </div>
                </div>

                <div className="whatido__card">
                    <div className="whatido__card__icon">
                        <FaDesktop />
                    </div>
                    <div className="whatido__card__text">
                        <h3>Desktop</h3>
                        Java, VB.Net
                    </div>
                </div>
            </div>

        </div>
    )
}

export default WhatIDo
