const BASE_URL = "http://localhost:5000"

export default{
    studies : `${BASE_URL}/studies`,
    work: `${BASE_URL}/work`,
    portfolio: `${BASE_URL}/portfolio`,
}