export const workData = [
    {
        _id: "work1",
        jobTitle: "Scientific Research Scholarship",
        company: "University of Aveiro - School of Technology and Management at Águeda",
        description: "Developed a web platform to manage smart lockers, users, user groups, permissions, timeschedules, configurations and a communication API. this platform was made using: Laravel, Bootstrap, Javascript, JQuery, Datatables, FullCalender and phpUnit.",
        startDate: "2020-03-1",
        skills: ['HTML', 'CSS', 'Bootstrap', 'JavaScript', 'JQuery', 'FullCalender', 'DataTables','PHP','Laravel','PHPUnit','API','REST','PostgreSQL']
    },
    {
        _id: "work2",
        jobTitle: "Scientific Research Initiation Scholarship",
        company: "University of Aveiro - School of Technology and Management at Águeda",
        description: "Deveoped a software for a gas vending machine for a company. This software was writen in Java for a Raspberry Pi and had to be able to comunicate through serial to a Nextion display  and a Arduino. Developed also the API of the platform and the comunication between this API and and the Raspberry Pi.",
        startDate: "2018-11-1",
        endDate: "2019-12-31",
        skills: ['Java','Serial Communications','Nextion Displays']
    },
    {
        _id: "work3",
        jobTitle: "Developer Internship",
        company: "Instituto de Telecomunicações (IT)",
        description: "Developed a web plaform with the intention of managing sheeps and monitoring their movement and activitie. this platform was made using Laravel, Javascript,JQuery, AJAX, JSON, PostgreSQL, Leaflet, Datatables and ChartJS.",
        startDate: "2019-2-1",
        endDate: "2019-6-15",
        skills: ['Laravel','Javascript','JQuery','AJAX','JSON','PostgreSQL','Leaflet','Datatables','ChartJS']
    },
    {
        _id: "work4",
        jobTitle: "Computer Technician Internship",
        company: "Maxima Informática, Lda",
        description: "Developed skills in building and building and maintaining a computeres and other hardware equipment.",
        startDate: "2016-1-5",
        endDate: "2016-3-1",
        skills: ['Windows', 'Computer Building', 'Computer maintenance']
    },
    {
        _id: "work5",
        jobTitle: "Hardware and Software Technician Internship",
        company: "Centro Social de Oiã",
        description: "Maintenance of computer hardware. Developed an inhouse software for managing equipment. Developed an inhouse software for managing order",
        startDate: "2015-5-1",
        endDate: "2015-6-1",
        skills:['VB.NET','Microsoft Access']
    },
];



export const studyData = [
    {
        _id: "study1",
        curseName: "Inforamtion Technology - Licentiate degree (level 6)",
        place: "University of Aveiro - School of Technology and Management at Águeda",
        description: "Developed previous skills and new skills in: Computer Architecture, Operating Systems, Data Structures, GIS Applications, Android Development, Human-Computer Interaction, Web Design, Cybersecurity Basics, Integration Technologies, Entrepreneurship, Management and Project Planning.",
        startDate: "2018-09-15",
        skills: ['Computer Architecture','Operating Systems','Data Structures','GIS Applications','Android Development','Human-Computer Interaction','Web Design','Cybersecurity Basics','Integration Technologies','Entrepreneurship','Management','Project Planning']
    },
    {
        _id: "study2",
        curseName: "Informations System Programming - Diploma of Higher Education (level 5)",
        place: "University of Aveiro - School of Technology and Management at Águeda",
        description: "Perform analysis and development of software, modules and web applications using serveral paradigms. Developed Skills in: C, Java, PostgresSQL, HTML, CSS, Bootstrap, JavaScript, PHP, Ajax, Apache, Web Services, REST, SOAP, Agile, Scrum, OOP, TDD, JUnit, Software Engineering, Prototyping, GIT and Teamwork.",
        startDate: "2016-09-15",
        endDate: "2019-06-15",
        skills: ['C','Java','PostgresSQL','HTML','CSS','Bootstrap','JavaScript','PHP','Ajax','Apache','Web Services','REST','SOAP','Agile','Scrum','OOP','TDD','JUnit','Software Engineering','Prototyping','GIT','Teamwork']
    },
    {
        _id: "study3",
        curseName: "Management and Computers - Certificate of Higher Education (level 4)",
        place: "Institudo de Promoção Social da Bairrada",
        description: "Gain basic knowledge of computer programming, networking, computer systems, Microsoft office, management and accouting. Developed skills in: Pascal, VB.NET, Game Development, Object-Oriented Programing, Event-Driven Programing, HTML, CSS, PHP, MySQL, Word, Excel, Access, Powerpoint, Accounting and Management.",
        startDate: "2013-09-15",
        endDate: "2016-06-15",
        skills: ['Pascal','VB.NET','Game Development','Object-Oriented Programming','Event-Driven Programing','HTML','CSS','PHP','MySQL','Word','Excel','Access','Powerpoint','Accounting','Management']
    },
];


export const portfolioData = [
    {
        appName : "Elock Android",
        year:   "2020",
        description : "eLock Android is Android application developed for final project of Mobile devices development class. the golad of this project is to create an android application that would connect to a backend API that would have the list of digital keys taht a user can use to access doors of buildings. This application also had to be able to comunicate with the doors using NFC or Bluetooth.",
        link : "",
        repoLink : "https://gitlab.com/dmos/elock",
        image: "/images/elock/elock.png"
    },
    {
        appName : "BedBuzz",
        year:   "2019/2020",
        description : "BedBuzz is a responsive web application developed to work with an electrical system created by students of electrical engineering of University of Aveiro. this project would allow to configure sensors of heart rate and electromyography of bedridden people with high physical limitations.",
        link : "",
        repoLink : "https://gitlab.com/dmos/bedbuzz",
        image: "/images/bedbuzz/bedbuzz.png"
    },
    {
        appName : "MyUA",
        year:   "2019",
        description : "MyUA is a backend web application developed to manage incoming reported problems in university campus by thier students. This project was a proof of concept to the university that using GIS and WEB tecnology together with the community, there is a way to make the campus better.",
        link : "",
        repoLink : "https://gitlab.com/dmos/myua",
        image: "/images/myua/myua.png"
    },
    {
        appName : "SheepIT Web",
        year:   "2018",
        description : "The SheepIT Web Platform is a responsive web application that aims to allow the monitoring Sheeps, by using data from various IoT sensores. This platform must be able to create, edit and view information related to sheep's, collar's, beacon's, properties, among others, using tables and graphs. Its also possible to delimit the areas of a registered property and visualize in real time sheep’s movement inside that area using a map.",
        link : "",
        repoLink : "https://gitlab.com/dmos/sheepit-web",
        image: "/images/sheepit/sheepit.png"
    },
    {
        appName : "DriveAway",
        year:   "2017/2018",
        description : "DriveAway is a responsive web application developed to help driving schools manage students and instructures. The main requirements of the application consists of management and basic operations of a driving school, it’s students and driving instructors.",
        link : "",
        repoLink : "https://gitlab.com/dmos/driveaway-web",
        image: "/images/driveaway/driveaway.png"
    },
]